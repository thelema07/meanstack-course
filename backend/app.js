
const express = require('express');

const app = express();

app.use('/api/posts',(req,res,next) => {
    const posts = [
        {
            id: 'df8sdfs999f6fs5fd4sf7sf9s',
            title: 'First backend service',
            constent: 'from the server'
        },
        {
            id: '23423hjhuui42uy4iu23y4sofu0f8s0d9f8',
            title: 'Second backend service',
            constent: 'from the server, again'
        }
    ];
    res.status(200).json({
        message: 'Post fetched',
        posts: posts
    });
});

module.exports = app;